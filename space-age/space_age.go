package space

import (
	"fmt"
	"math"
)

type Planet string

var planetYears = map[Planet]float64{
	"Mercury": 0.2408467,
	"Venus":   0.61519726,
	"Earth":   1.0,
	"Mars":    1.8808158,
	"Jupiter": 11.862615,
	"Saturn":  29.447498,
	"Uranus":  84.016846,
	"Neptune": 164.79132,
}

var earthYear = 31557600.0

// Age calculates a person's age on a given planet
func Age(seconds float64, planet Planet) float64 {
	yearLength := earthYear * planetYears[planet]
	fmt.Println(yearLength)
	age := seconds / yearLength
	return Round(age, .5, 2)
}

// Round rounds a floating point number.
// Found at https://play.golang.org/p/yjfShH_uEy
func Round(val float64, roundOn float64, places int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit)
	if div >= roundOn {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}
	newVal = round / pow
	return
}

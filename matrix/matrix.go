package matrix

import (
	"errors"
	"strconv"
	"strings"
)

type Matrix [][]int

func New(input string) (matrix Matrix, err error) {
	rows := strings.Split(input, "\n")
	numRows := len(rows)
	numCols := len(strings.Split(rows[0], " "))
	matrix = make([][]int, numRows)
	for i := 0; i < numRows; i++ {
		matrix[i] = make([]int, numCols)
		currentRow := strings.Trim(rows[i], " ")
		data := strings.Split(currentRow, " ")
		if len(data) != numCols {
			err = errors.New("Uneven rows")
			return
		}
		for j := 0; j < numCols; j++ {
			var dataPoint64 int64
			dataPoint64, err = strconv.ParseInt(data[j], 10, 64)
			if err != nil {
				return
			}
			dataPoint := int(dataPoint64)
			matrix.Set(i, j, dataPoint)
		}
	}
	return matrix, nil
}

func (matrix *Matrix) Cols() [][]int {
	numRowsOrig := len(*matrix)
	if numRowsOrig == 0 {
		return make([][]int, 0)
	}
	numColsOrig := len((*matrix)[0])
	numRowsNew := numColsOrig
	numColsNew := numRowsOrig
	copyCols := make([][]int, numRowsNew)
	for i := 0; i < numRowsNew; i++ {
		copyCols[i] = make([]int, numColsNew)
		for j := 0; j < numColsNew; j++ {
			copyCols[i][j] = (*matrix)[j][i]
		}
	}
	return copyCols
}

func (matrix *Matrix) Rows() [][]int {
	numRows := len(*matrix)
	if numRows == 0 {
		return make([][]int, 0)
	}
	numCols := len((*matrix)[0])
	copyRows := make([][]int, numRows)
	for i := 0; i < numRows; i++ {
		copyRows[i] = make([]int, numCols)
		for j := 0; j < numCols; j++ {
			copyRows[i][j] = (*matrix)[i][j]
		}
	}
	return copyRows
}

func (matrix *Matrix) Set(row, col, val int) (status bool) {
	defer func() {
		if r := recover(); r != nil {
			status = false
		}
	}()
	(*matrix)[row][col] = val
	return true
}

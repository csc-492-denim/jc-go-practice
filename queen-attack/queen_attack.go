package queenattack

import (
	"errors"
	"math"
)

const minColumn = 97
const maxColumn = 104
const minRow = 49
const maxRow = 56

type Position struct {
	column, row rune
}

func CanQueenAttack(white, black string) (result bool, err error) {
	whitePosition, err := parsePosition(white)
	if err != nil {
		return false, err
	}
	blackPosition, err := parsePosition(black)
	if err != nil {
		return false, err
	}
	err = ensurePositionsNotEqual(whitePosition, blackPosition)
	if err != nil {
		return false, err
	}
	canAttack := checkPositions(whitePosition, blackPosition)
	return canAttack, nil
}

func parsePosition(data string) (position Position, err error) {
	if len(data) != 2 {
		return Position{}, errors.New("expected position to have length 2")
	}

	const columnIndex = 0
	const rowIndex = 1

	position.column = []rune(data)[columnIndex]
	position.row = []rune(data)[rowIndex]
	err = validatePositionOnBoard(position)
	if err != nil {
		return Position{}, err
	}

	return position, nil
}

func validatePositionOnBoard(position Position) error {
	if position.column < minColumn || position.column > maxColumn {
		return errors.New("position not on board")
	}
	if position.row < minRow || position.row > maxRow {
		return errors.New("position not on board")
	}
	return nil
}

func ensurePositionsNotEqual(whitePos, blackPos Position) error {
	if whitePos.column == blackPos.column && whitePos.row == blackPos.row {
		return errors.New("positions cannot be equal")
	}
	return nil
}

func checkPositions(white, black Position) bool {
	if white.column == black.column {
		return true
	}
	if white.row == black.row {
		return true
	}
	rowDiff := math.Abs(float64(white.row - black.row))
	colDiff := math.Abs(float64(white.column - black.column))
	if rowDiff == colDiff {
		return true
	}
	return false
}

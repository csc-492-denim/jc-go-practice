// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package bob should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package bob

import (
	"strings"
	"unicode"
)

// Hey should have a comment documenting it.
func Hey(remark string) string {
	// Write some code here to pass the test suite.
	// Then remove all the stock comments.
	// They're here to help you get started but they only clutter a finished solution.
	// If you leave them in, reviewers may protest!

	if isSilence(remark) {
		return "Fine. Be that way!"
	}

	if isAngryQuestion(remark) {
		return "Calm down, I know what I'm doing!"
	}

	if isAngry(remark) {
		return "Whoa, chill out!"
	}

	if isQuestion(remark) {
		return "Sure."
	}

	return "Whatever."
}

func isSilence(remark string) bool {
	return strings.Trim(remark, " \t\n\r") == ""
}

func containsLetters(remark string) bool {
	for _, r := range remark {
		if unicode.IsLetter(r) {
			return true
		}
	}
	return false
}

func isAngry(remark string) bool {
	if !containsLetters(remark) {
		return false
	}
	return strings.ToUpper(remark) == remark
}

func isQuestion(remark string) bool {
	remark = strings.Trim(remark, " ")
	return strings.LastIndex(remark, "?") == len(remark)-1
}

func isAngryQuestion(remark string) bool {
	return isAngry(remark) && isQuestion(remark)
}
